﻿using DemoLibrary;

namespace ConsoleUI;

class Program
{
    static void Main(string[] args)
    {
        Person owner = new Person
        {
            FirstName = "Dillon",
            LastName = "Cooper",
            EmailAddress = "dillon.cooper@example.com",
            PhoneNumber = "555-1234"
        };

        Chore chore = new Chore()
        {
            ChoreName = "Take out the trash",
            Owner = owner
        };

        chore.PerformedWork(3);
        chore.PerformedWork(1.5);
        chore.CompleteChore();
        
        Console.ReadLine();
    }
}
